//
//  NewOrder.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class NewOrder: UIViewController {

    var token = ""
    
    @IBOutlet weak var pointAlbl: UITextField!
    @IBOutlet weak var pointBlbl: UITextField!
    @IBOutlet weak var costLbl: UITextField!
    @IBOutlet weak var primLbl: UITextField!
    
    @IBOutlet weak var imgPointA: UIImageView!
    @IBOutlet weak var imgPointB: UIImageView!
    @IBOutlet weak var imgCost: UIImageView!
    @IBOutlet weak var imgPrim: UIImageView!
    
    @IBOutlet weak var bonusBtn: UIBarButtonItem!
    @IBOutlet weak var payByBonus: UIButton!
    
    var balance = 0
    var minSumm = 0
    
    @IBOutlet weak var connectorLine: UIView!
    
    var isPayByBonus = false
    
    @IBOutlet weak var addOrderBtn: UIButton!
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addOrderBtn.setTopLine()
        
        self.addDoneButtonOnKeyboard()
        
        if let token = UserDefaults.standard.string(forKey: "token") {
            self.token = token
        }
        
        self.balance = UserDefaults.standard.integer(forKey: "balance")
        self.minSumm = UserDefaults.standard.integer(forKey: "minDiscountSum")
        
        self.bonusBtn.title = "\(self.balance)"
        
        if let pointA = UserDefaults.standard.string(forKey: "pointA") {
            self.pointAlbl.text = pointA
        }
        
        if let pointB = UserDefaults.standard.string(forKey: "pointB") {
            self.pointBlbl.text = pointB
        }
        
        if let cost = UserDefaults.standard.string(forKey: "cost") {
            self.costLbl.text = cost
        }
        
        if let prim = UserDefaults.standard.string(forKey: "prim") {
            self.primLbl.text = prim
        }
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backButtonItem
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        if let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardFrame.size.height + 60
            print("keyboard height: \(keyboardHeight)")
            self.buttonConstraint.constant = -keyboardHeight
            self.view.layoutIfNeeded()
            
            let keyboardEndFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let animationCurveRawNSN = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve = UIView.AnimationCurve(rawValue: Int(notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? Int ?? 0))
            let animationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            let animationDuration = (notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            
            let keyboardEndFrameLocation = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.origin
            let screen =  UIScreen.main.bounds
            let newBottomConstraint = screen.size.height - (keyboardEndFrameLocation?.y)!
            
            let oldYinputPos = self.addOrderBtn.bounds.origin.y
            
            UIView.animate(withDuration: animationDuration, delay: 0, options: animationOptions, animations: {
                
                self.buttonConstraint.constant = newBottomConstraint
                self.view.layoutIfNeeded()
                
                //self.inputMessageView.bounds.origin = newPos
                
            }, completion: nil)
            
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar()
        
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        
        items.append(flexSpace)
        
        items.append(done)
        
        doneToolbar.items = items
        
        doneToolbar.sizeToFit()
        
        self.costLbl.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.costLbl.resignFirstResponder()
    }
    
    @IBAction func addOrderTapped(_ sender: UIButton) {
        if !checkForErrors() {
            
            self.sendOrder(token: self.token, pointA: self.pointAlbl.text!, pointB: self.pointBlbl.text!, cost: self.costLbl.text!, prim: self.primLbl.text!)
            
        }
    }
    
    func checkForErrors() -> Bool {
    
        let errDial = ErrorDialog()
        
        if self.pointAlbl.text!.count == 0 {
            errDial.showErrorDialog(err_id: 41, viewController: self, needRetry: false)
            return true
        }
        
        if self.pointBlbl.text!.count == 0 {
            errDial.showErrorDialog(err_id: 42, viewController: self, needRetry: false)
            return true
        }
        
        if self.costLbl.text!.count == 0 {
            errDial.showErrorDialog(err_id: 43, viewController: self, needRetry: false)
            return true
        }
        
        return false
    }
    
    func sendOrder(token: String, pointA: String, pointB: String, cost: String, prim: String) {
        
        var discount_sum = ""
        if self.isPayByBonus {
            discount_sum = cost
        } else {
            discount_sum = "0"
        }
        
        Interactor().makeRequest(funcName: "addorder", requestParams: ["token": token, "point_a": pointA, "point_b": pointB, "price": cost, "prim": prim, "payment_id": "0"], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    let order_id = Int(String(describing: jsonObj!["order_id"] as! NSObject))!
                    
                    UserDefaults.standard.set(pointA, forKey: "pointA")
                    UserDefaults.standard.set(pointB, forKey: "pointB")
                    UserDefaults.standard.set(cost, forKey: "cost")
                    UserDefaults.standard.set(prim, forKey: "prim")
                    
                    let orderSearchDriver = self.storyboard?.instantiateViewController(withIdentifier: "OrderSearchDriver") as! UINavigationController
                    let rootView = orderSearchDriver.viewControllers.first as! OrderSearchDriver
                    rootView.order_id = order_id
                    rootView.isFromLogin = false
                    self.present(orderSearchDriver, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func payByBonusTapped(_ sender: UIButton) {
        if !self.isPayByBonus  {
            
            if let cost = Int(self.costLbl.text!) {
            
                if cost >= minSumm {
                    if balance >= cost {
                        
                        self.isPayByBonus = true
                        self.payByBonus.setImage( UIImage(named: "ic_bonus_yellow"), for: .normal)
                        
                    } else {
                        
                        ErrorDialog().showErrorDialog(err_id: 31, viewController: self, needRetry: false)
                        
                    }
                    
                } else {
                    
                    ErrorDialog().showErrorDialog(err_id: 32, viewController: self, needRetry: false)
                    
                }
            }
            
        } else {
            self.isPayByBonus = false
            self.payByBonus.setImage( UIImage(named: "ic_bonus_gray"), for: .normal) 
        }
        
        print("isBonus: \(self.isPayByBonus)")
    }
    
    
    @IBAction func bonusTapped(_ sender: UIBarButtonItem) {
        self.openBinusInfo()
    }
    
    @IBAction func bonusIconTapped(_ sender: UIBarButtonItem) {
        self.openBinusInfo()
    }
    
    func openBinusInfo() {
        let bonus = self.storyboard?.instantiateViewController(withIdentifier: "BonusInfo") as! BonusInfo
        self.navigationController?.pushViewController(bonus, animated: true)
    }
    
    @IBAction func settingsTapped(_ sender: UIBarButtonItem) {
        let settings = self.storyboard?.instantiateViewController(withIdentifier: "UserSettings") as! UserSettings
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    @IBAction func callTapped(_ sender: UIBarButtonItem) {
        
        guard let number = URL(string: "tel://+79142700006") else { return }
        UIApplication.shared.open(number)
        
    }
}

extension NewOrder: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.checkValues()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.checkValues()
    }
    
    func checkValues() {
        if self.pointAlbl.text!.count > 0 && self.pointBlbl.text!.count > 0 {
            self.connectorLine.backgroundColor = UIColor(red: 255.0 / 255.0, green: 193.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
        } else {
            self.connectorLine.backgroundColor = UIColor(red: 173.0 / 255.0, green: 173.0 / 255.0, blue: 173.0 / 255.0, alpha: 1.0)
        }
        
        if self.pointAlbl.text!.count > 0 {
            self.imgPointA.image = UIImage(named: "a_enabled")
        } else {
            self.imgPointA.image = UIImage(named: "a_disabled")
        }
        
        if self.pointBlbl.text!.count > 0 {
            self.imgPointB.image = UIImage(named: "b_enabled")
        } else {
            self.imgPointB.image = UIImage(named: "b_disabled")
        }
        
        if self.costLbl.text!.count > 0 {
            self.imgCost.image = UIImage(named: "rub_enabled")
        } else {
            self.imgCost.image = UIImage(named: "rub_disabled")
        }
        
        if self.primLbl.text!.count > 0 {
            self.imgPrim.image = UIImage(named: "comment_enabled")
        } else {
            self.imgPrim.image = UIImage(named: "comment_disabled")
        }
    }
    
}
