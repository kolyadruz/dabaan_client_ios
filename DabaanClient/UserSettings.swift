//
//  UserSettings.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class UserSettings: UIViewController {

    var token = ""
    
    @IBOutlet weak var fioField: UITextField!
    @IBOutlet weak var cityBtn: UIButton!
    
    var cityId = "1"
    var cityName = ""
    
    override func viewDidLoad() {
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backButtonItem
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let token = UserDefaults.standard.string(forKey: "token") {
            self.token = token
        }
        
        self.loadUserInfo(token: self.token)
        
        if let cityName = UserDefaults.standard.string(forKey: "cityName") {
            
            self.cityBtn.titleLabel?.text = cityName
            self.cityBtn.layoutIfNeeded()
            self.cityName = cityName
        }
        
        if let cityId = UserDefaults.standard.string(forKey: "cityId") {
            self.cityId = cityId
        }
    }
    
    func loadUserInfo(token: String) {
        
        Interactor().makeRequest(funcName: "load_userinfo", requestParams: ["token": token], delegate: self) { jsonObj in
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    self.fioField.text = jsonObj!["name"] as! String
                    
                }
                
            }
        }
        
    }
    
    @IBAction func themeTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func cityTapped(_ sender: UIButton) {
        let cityChooser = self.storyboard?.instantiateViewController(withIdentifier: "CityChooser") as! CityChooser
        cityChooser.isFromSettings = true
        self.navigationController?.pushViewController(cityChooser, animated: true)
    }
    
    @IBAction func exitTapped(_ sender: UIButton) {
        UserDefaults.standard.set("", forKey: "token")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        
        Interactor().makeRequest(funcName: "ch_userinfo", requestParams: ["token": self.token, "name": self.fioField.text!], delegate: self) { jsonObj in
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
        }
        
    }
    

}

extension UserSettings: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
