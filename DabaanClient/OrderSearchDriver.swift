//
//  OrderSearchDriver.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class OrderSearchDriver: UIViewController {

    var token = ""
    
    var order_id = 0
    var isCanceled = false
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    
    @IBOutlet weak var pointAlbl: UILabel!
    @IBOutlet weak var pointBlbl: UILabel!
    @IBOutlet weak var costLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    
    @IBOutlet weak var imgCost: UIImageView!
    
    var status = 0
    var driverPhone = ""
    
    var isFromLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateBtn.isHidden = true
        
        self.cancelBtn.setTopLine()
        self.updateBtn.setTopLine()
        
        if let token = UserDefaults.standard.string(forKey: "token") {
            self.token = token
        }
        
        print("active_order_id: \(self.order_id)")
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backButtonItem
    }

    override func viewDidAppear(_ animated: Bool) {
        self.checkOrder(token: self.token)
    }
    
    func checkOrder(token: String) {
        
        Interactor().makeRequest(funcName: "checkorder", requestParams: ["token": token, "order_id":"\(self.order_id)"], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    let status = Int(String(describing: jsonObj!["status"] as! NSObject))!
                    
                    let point_a = jsonObj!["point_a"] as! String
                    let point_b = jsonObj!["point_b"] as! String
                    let prim = jsonObj!["prim"] as! String
                    
                    let price = jsonObj!["price"] as! Int
                    let discount_sum = Int(String(describing: jsonObj!["discount_sum"] as! NSObject))!
                    
                    if price > discount_sum {
                        self.imgCost.image = UIImage(named: "rub_enabled")
                        self.costLbl.text = "\(price)"
                    } else {
                        self.imgCost.image = UIImage(named: "rub_enabled")
                        self.costLbl.text = "\(discount_sum) (бонусами)"
                    }
                    
                    self.pointAlbl.text = point_a
                    self.pointBlbl.text = point_b
                    self.commentLbl.text = prim
                    
                    switch status {
                    case 1:
                        self.statusLbl.text = "Поиск водителя..."
                        self.detailsLbl.text = ""
                        
                        self.runCheckOrderWithInterval()
                    case 2:
                        self.statusLbl.text = "Ваш заказ принят"
                        
                        let name = jsonObj!["name"] as! String
                        let car_model = jsonObj!["car_model"] as! String
                        let car_color = jsonObj!["car_color"] as! String
                        let car_number = jsonObj!["car_number"] as! String
                        
                        self.driverPhone = jsonObj!["phone"] as! String
                        
                        let dtail = "К Вам подъедет \(name) на \(car_model) \(car_color) с гос.номером: \(car_number)"
                        
                        self.detailsLbl.text = dtail
                        
                        self.runCheckOrderWithInterval()
                    case 3:
                        
                        let orderCompleted = self.storyboard?.instantiateViewController(withIdentifier: "OrderCompleted") as! OrderCompleted
                        self.present(orderCompleted, animated: true, completion: nil)
                        
                    case 5:
                        self.statusLbl.text = "Ваш заказ отменен"
                        self.cancelBtn.titleLabel?.text = "Закрыть"
                        self.cancelBtn.layoutIfNeeded()
                        self.detailsLbl.text = "Водитель отменил Ваш заказ"
                        
                        self.isCanceled = true
                    case 8:
                        self.statusLbl.text = "Ваш заказ отменен"
                        self.cancelBtn.titleLabel?.text = "Закрыть"
                        self.cancelBtn.layoutIfNeeded()
                        self.detailsLbl.text = "Диспетчер отменил Ваш заказ"
                        
                        self.isCanceled = true
                    case 10:
                        let orderCompleted = self.storyboard?.instantiateViewController(withIdentifier: "OrderCompleted") as! OrderCompleted
                        self.present(orderCompleted, animated: true, completion: nil)
                    default:
                        break
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func runCheckOrderWithInterval() {
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runCheckOrder), userInfo: nil, repeats: false)
        
    }
    
    @objc func runCheckOrder() {
        self.checkOrder(token: self.token)
    }
 
    @IBAction func cancelTapped(_ sender: UIButton) {
        Interactor().makeRequest(funcName: "cancelorder", requestParams: ["token": self.token, "order_id":"\(self.order_id)", "reason_id":"1"], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    if self.isFromLogin {
                        let newOrder = self.storyboard?.instantiateViewController(withIdentifier: "NewOrder") as! UINavigationController
                        self.present(newOrder, animated: true, completion: nil)
                    } else {
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    @IBAction func updateTapped(_ sender: UIButton) {
        Interactor().makeRequest(funcName: "updateorder", requestParams: ["token": self.token, "order_id":"\(self.order_id)"], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                }
            }
        }
    }
    
    @IBAction func settingsTapped(_ sender: UIBarButtonItem) {
        
        let settings = self.storyboard?.instantiateViewController(withIdentifier: "UserSettings") as! UserSettings
        self.navigationController?.pushViewController(settings, animated: true)
        
    }
    
    @IBAction func callTapped(_ sender: UIBarButtonItem) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Позвонить", message: "Выберите абонента для звонка", preferredStyle: .actionSheet)
        
        if self.driverPhone != "" {
            let callToDriver = UIAlertAction(title: "Водителю", style: .default) { _ in
                
                guard let number = URL(string: "tel://+\(self.driverPhone)") else { return }
                UIApplication.shared.open(number)
                
            }
            actionSheetController.addAction(callToDriver)
        }
        
        let callToDisp = UIAlertAction(title: "Диспетчеру", style: .default) { _ in
            
            guard let number = URL(string: "tel://+79142700006") else { return }
            UIApplication.shared.open(number)
            
        }
        actionSheetController.addAction(callToDisp)
        
        let close = UIAlertAction(title: "Отмена", style: .cancel) { _ in
            
        }
        actionSheetController.addAction(close)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
}
