//
//  Login.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import Alamofire

class Login: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        if let token = UserDefaults.standard.string(forKey: "token") {
            
            print("token found: \(token)")
            
            /*if token != "" {
                
                print("token found: \(token)")*/
                
                self.checkLogin(token: token)
            /*} else {
                
                print("token found but empty")
                DispatchQueue.main.async {
                    let chooser = self.storyboard?.instantiateViewController(withIdentifier: "Chooser") as! Chooser
                    self.present(chooser, animated: true, completion: nil)
                }
            }*/
            
        } else {
            
            print("token NOT found")
            DispatchQueue.main.async {
                let chooser = self.storyboard?.instantiateViewController(withIdentifier: "Chooser") as! Chooser
                self.present(chooser, animated: true, completion: nil)
            }
        }
    }
    
    func checkLogin(token: String) {
        
        Interactor().makeRequest(funcName: "clientlogin", requestParams: ["token": token, "fb_token": " "], delegate: self) { jsonObj in
        
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    let order_id = Int(String(describing: jsonObj!["order_id"] as! NSObject))!
                    let status = Int(String(describing: jsonObj!["status"] as! NSObject))!
                
                    let city = jsonObj!["city"] as! NSDictionary
                
                    let cityName = city["name"] as! String
                    let cityId = city["id"] as! String
                    let minDiscountSum = jsonObj!["min_discount_sum"] as! Int
                    let balance = Int(String(describing: jsonObj!["balance"] as! NSObject))!
                
                    UserDefaults.standard.set(cityName, forKey: "cityName")
                    UserDefaults.standard.set(cityId, forKey: "cityId")
                    UserDefaults.standard.set(minDiscountSum, forKey: "minDiscountSum")
                    UserDefaults.standard.set(balance, forKey: "balance")
                
                    print("status: \(status)")
                    
                    if status == 4 || status == 6 || status == 10 {
                        
                        let newOrder = self.storyboard?.instantiateViewController(withIdentifier: "NewOrder") as! UINavigationController
                        self.present(newOrder, animated: true, completion: nil)
                        
                    } else {
                        
                        let orderSearchDriver = self.storyboard?.instantiateViewController(withIdentifier: "OrderSearchDriver") as! UINavigationController
                        let rootView = orderSearchDriver.viewControllers.first as! OrderSearchDriver
                        rootView.order_id = order_id
                        rootView.isFromLogin = true
                        self.present(orderSearchDriver, animated: true, completion: nil)
                        
                    }
                
                }
            
            } else {
                
                DispatchQueue.main.async {
                    let chooser = self.storyboard?.instantiateViewController(withIdentifier: "Chooser") as! Chooser
                    self.present(chooser, animated: true, completion: nil)
                }
                
            }
        }
            
    }
    
}
