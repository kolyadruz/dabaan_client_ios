//
//  ChooserCell.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 17/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

protocol ChooserCellDelegate {
    
    func enterTapped()
}

class ChooserCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var logoLbl: UILabel!
    @IBOutlet weak var underLogoLbl: UILabel!
    @IBOutlet weak var underLogoSubLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var titleSubLbl: UILabel!
    @IBOutlet weak var enterBtn: UIButton!
    
    var delegate: ChooserCellDelegate?
    
    var page: NSDictionary? {
        
        didSet {
            
            self.imgView.image = UIImage(named: page!["image"] as! String)
            
            if page!["isFirst"] as! Int == 1 {
                
                self.logoLbl.isHidden = false
                self.underLogoLbl.isHidden = false
                self.underLogoSubLbl.isHidden = false
                self.titleLbl.isHidden = true
                self.titleSubLbl.isHidden = true
                self.underLogoLbl.text = page!["title"] as! String
                self.underLogoSubLbl.text = page!["subtitle"] as! String
                
            } else {
                
                self.logoLbl.isHidden = true
                self.underLogoLbl.isHidden = true
                self.underLogoSubLbl.isHidden = true
                self.titleLbl.isHidden = false
                self.titleSubLbl.isHidden = false
                self.titleLbl.text = page!["title"] as! String
                self.titleSubLbl.text = page!["subtitle"] as! String
                
            }
            
            if page!["isLast"] as! Int == 1 {
                self.enterBtn.isHidden = false
            } else {
                self.enterBtn.isHidden = true
            }
            
        }
        
    }
    
    override public func draw(_ rect: CGRect) {
        
        self.enterBtn.addTarget(self, action: #selector(enterTapped), for: .touchUpInside)
        
    }
    
    @objc func enterTapped(sender: UIButton) {
        
        self.delegate?.enterTapped()
    }
    
}
