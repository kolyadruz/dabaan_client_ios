//
//  Hash.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class Hash: UIViewController {

    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var hashField: UITextField!
    
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkBtn.setTopLine()
        
        if let phone = UserDefaults.standard.string(forKey: "phone") {
            self.phoneLbl.text = phone
        }
        
        addDoneButtonOnKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        if let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardFrame.size.height + 60
            print("keyboard height: \(keyboardHeight)")
            self.buttonConstraint.constant = -keyboardHeight
            self.view.layoutIfNeeded()
            
            let keyboardEndFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let animationCurveRawNSN = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve = UIView.AnimationCurve(rawValue: Int(notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? Int ?? 0))
            let animationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            let animationDuration = (notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            
            let keyboardEndFrameLocation = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.origin
            let screen =  UIScreen.main.bounds
            let newBottomConstraint = screen.size.height - (keyboardEndFrameLocation?.y)!
            
            let oldYinputPos = self.checkBtn.bounds.origin.y
            
            UIView.animate(withDuration: animationDuration, delay: 0, options: animationOptions, animations: {
                
                self.buttonConstraint.constant = newBottomConstraint
                self.view.layoutIfNeeded()
                
                //self.inputMessageView.bounds.origin = newPos
                
            }, completion: nil)
            
        }
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar()
        
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        
        items.append(flexSpace)
        
        items.append(done)
        
        doneToolbar.items = items
        
        doneToolbar.sizeToFit()
        
        self.hashField.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.hashField.resignFirstResponder()
    }
    
    @IBAction func checkTapped(_ sender: UIButton) {
        self.takeCode(phone: self.phoneLbl.text!, hash: self.hashField.text!)
    }
    
    func takeCode(phone: String, hash: String) {
        
        Interactor().makeRequest(funcName: "checkhash", requestParams: ["phone": phone, "hash": hash], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    UserDefaults.standard.set(jsonObj!["token"] as! String, forKey: "token")
                    UserDefaults.standard.set(jsonObj!["min_discount_sum"] as! Int, forKey: "minDiscountSum")
                    UserDefaults.standard.set(Int(jsonObj!["balance"] as! String), forKey: "balance")
                    
                    let cityChooser = self.storyboard?.instantiateViewController(withIdentifier: "CityChooser") as! CityChooser
                    self.present(cityChooser, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }
    
}
