//
//  CityChooser.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class CityChooser: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var citySpinner: UIPickerView!
    
    var token = ""
    var currentCityID = "1"
    
    var cities = [NSDictionary]()
    
    var isFromSettings = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let token = UserDefaults.standard.string(forKey: "token") {
            self.token = token
        }
        
        if let currentCityID = UserDefaults.standard.string(forKey: "cityId") {
            self.currentCityID = currentCityID
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getCities(token: self.token)
    }

    func getCities(token: String) {
        
        Interactor().makeRequest(funcName: "getcities", requestParams: ["token":token], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    self.cities.removeAll()
                    let citiesArray = jsonObj!["cities"] as! NSArray
                    
                    for city in citiesArray {
                        self.cities.append(city as! NSDictionary)
                    }
                    
                    self.citySpinner.reloadAllComponents()
                }
                
            }
            
        }
        
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        self.setCity(token: self.token)
    }
    
    func setCity(token: String) {
        
        let city = self.cities[self.citySpinner.selectedRow(inComponent: 0)]
        
        let id = city["id"] as! String
        let cityName = city["name"] as! String
        
        Interactor().makeRequest(funcName: "setcity", requestParams: ["token": token, "city":id], delegate: self) { jsonObj in
            
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    UserDefaults.standard.set(id, forKey: "cityId")
                    UserDefaults.standard.set(cityName, forKey: "cityName") 
                    
                    if self.isFromSettings {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        let newOrder = self.storyboard?.instantiateViewController(withIdentifier: "NewOrder") as! UINavigationController
                        self.present(newOrder, animated: true, completion: nil)
                    }
                    
                }
                
            }
            
        }
        
    }
}

extension CityChooser: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.cities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let city = self.cities[row]
        
        let name = city["name"] as! String
        
        return name
        
    }
    
}
