//
//  Chooser.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 17/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class Chooser: UIViewController, ChooserCellDelegate {
    
    func enterTapped() {
        DispatchQueue.main.async {
            let reg = self.storyboard?.instantiateViewController(withIdentifier: "Register") as! Register
            self.present(reg, animated: true, completion: nil)
        }
    }

    @IBOutlet weak var collectView: UICollectionView!
    
    var pages = [NSDictionary]()
    
    @IBOutlet weak var pageCtrl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (self.collectView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        
        (self.collectView.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = 0
        self.collectView.frame = .zero
        self.collectView.translatesAutoresizingMaskIntoConstraints = false
        self.collectView.isPagingEnabled = true
        self.collectView.showsHorizontalScrollIndicator = false
        
        let first: NSDictionary = ["title":"Таксомоторная служба", "subtitle":"в вашем кармане", "image":"car1.png", "isFirst": 1, "isLast": 0]
        let second: NSDictionary = ["title":"Быстрая подача авто.", "subtitle":"Опытные водители.", "image":"car2.png", "isFirst": 0, "isLast": 0]
        let third: NSDictionary = ["title":"Ничего лишнего.", "subtitle":"Только самое необходимое.", "image":"car3.png", "isFirst": 0, "isLast": 1]
        
        pages.append(first)
        pages.append(second)
        pages.append(third)
        
    }

}

extension Chooser: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.pages.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooserCell", for: indexPath) as! ChooserCell
        
        cell.page = self.pages[indexPath.item]
        cell.delegate = self
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        
        self.pageCtrl.currentPage = Int(pageIndex)
    }
    
}
