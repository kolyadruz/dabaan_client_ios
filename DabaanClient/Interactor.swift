//
//  Interactor.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 22/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import Alamofire

class Interactor {
    
    func makeRequest(funcName: String, requestParams: [String: Any], delegate: UIViewController, completion: @escaping (_ jsonObj: NSDictionary?) -> Void) {
        
        request(GlobalVars.api_url + funcName, method: .post, parameters: requestParams).responseJSON { response in
            
            print(response)
            
            guard let statusCode = response.response?.statusCode else { return }
            
            if (200..<300).contains(statusCode) {
            
                let jsonObj = response.result.value as! NSDictionary
                let err_id = jsonObj["err_id"] as! Int
                
                if err_id == 0 {
                    
                    completion(jsonObj)
                    return
                    
                } else {
                    
                    ErrorDialog().showErrorDialog(err_id: err_id, viewController: delegate, needRetry: false)
                    
                }
                
            } else {
                
                ErrorDialog().showErrorDialog(err_id: 20, viewController: delegate, needRetry: true)
                
            }
        }
        
        completion(nil)
        return
    }
    
}
