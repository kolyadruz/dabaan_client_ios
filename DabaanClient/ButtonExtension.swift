//
//  ButtonExtension.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 24/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setTopLine() {
        
        let mainViewSize = self.bounds.size
        let borderWidth: CGFloat = 1.0
        let null: CGFloat = 0.0
        let borderColor = UIColor(red: 223.0/255, green: 223.0/255, blue: 223.0/255, alpha: 1.0)
        let topView = UIView(frame: CGRect(x: null, y: null, width: mainViewSize.width, height: borderWidth))
        topView.isOpaque = true
        topView.backgroundColor = borderColor;
        topView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        self.addSubview(topView)
        
    }
    
}
