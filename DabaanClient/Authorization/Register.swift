//
//  Register.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class Register: UIViewController {
    
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmBtn.setTopLine()
        
        addDoneButtonOnKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }

    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        if let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardFrame.size.height + 60
            print("keyboard height: \(keyboardHeight)")
            self.buttonConstraint.constant = -keyboardHeight
            self.view.layoutIfNeeded()
            
            let keyboardEndFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let animationCurveRawNSN = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve = UIView.AnimationCurve(rawValue: Int(notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? Int ?? 0))
            let animationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            let animationDuration = (notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            
            let keyboardEndFrameLocation = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.origin
            let screen =  UIScreen.main.bounds
            let newBottomConstraint = screen.size.height - (keyboardEndFrameLocation?.y)!
            
            let oldYinputPos = self.confirmBtn.bounds.origin.y
            
            UIView.animate(withDuration: animationDuration, delay: 0, options: animationOptions, animations: {
                
                self.buttonConstraint.constant = newBottomConstraint
                self.view.layoutIfNeeded()
                
                //self.inputMessageView.bounds.origin = newPos
                
            }, completion: nil)
            
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar()
        
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        
        items.append(flexSpace)
        
        items.append(done)
        
        doneToolbar.items = items
        
        doneToolbar.sizeToFit()
        
        self.phoneField.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.phoneField.resignFirstResponder()
    }
    
    @IBAction func confirmTapped(_ sender: UIButton) {
        if self.phoneField.text!.count == 10 {
            self.takeCode(phone: "7" + phoneField.text!, name: nameField.text!)
        } else {
            ErrorDialog().showErrorDialog(err_id: 1, viewController: self, needRetry: false)
        }
    }
    
    func takeCode(phone: String, name: String) {
        
        Interactor().makeRequest(funcName: "reg", requestParams: ["phone": phone, "name": name], delegate: self) { jsonObj in
        
            print("taken \(jsonObj)")
            
            if jsonObj != nil {
                DispatchQueue.main.async {
                    
                    UserDefaults.standard.set(phone, forKey: "phone")
                    
                    let hash = self.storyboard?.instantiateViewController(withIdentifier: "Hash") as! Hash
                    self.present(hash, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }
    
}

extension Register: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
