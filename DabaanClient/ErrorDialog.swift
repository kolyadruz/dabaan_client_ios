//
//  ErrorDialog.swift
//  DabaanClient
//
//  Created by Nikolay Druzianov on 18/04/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class ErrorDialog {
    
    func showErrorDialog(err_id: Int, viewController: UIViewController, needRetry: Bool) {
        
        var err_message = ""
        
        let minSumm = UserDefaults.standard.integer(forKey: "minDiscountSum")
        
        switch err_id {
        case 1:
            err_message = "Указан неправильный номер телефона"
        case 2:
            err_message = "Повторите попытку позднее"
        case 3:
            err_message = "Ошибка работы токена"
        case 4:
            err_message = "Заполните все поля"
        case 5:
            err_message = "Ваш аккаунт неактивен"
        case 6:
            err_message = "Пользователь отсутствует или заблокирован"
        case 7:
            err_message = "Заказ закрыт, или отменен"
        case 8:
            err_message = "Недостаточно средств на балансе"
        case 9:
            err_message = "Заказ уже принят"
        case 20:
            err_message = "Ошибка интернет соединения"
        case 31:
            err_message = "У Вас недостаточно бонусных баллов"
        case 32:
            err_message = "Сумма проезда должна быть не менее \(minSumm) руб. для оплаты проезда бонусами."
        case 41:
            err_message = "Введите адрес отбытия"
        case 42:
            err_message = "Введите адрес прибытия"
        case 43:
            err_message = "Введите цену"
        default: break
        }
        
        let alert = UIAlertController(title: "Ошибка", message: err_message, preferredStyle: UIAlertController.Style.alert)
        
        if needRetry {
            alert.addAction(UIAlertAction(title: "Повторить", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                DispatchQueue.main.async {
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: UIAlertAction.Style.cancel, handler: { (action: UIAlertAction!) in
                DispatchQueue.main.async {
                    
                }
            }))
        } else {
            alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                DispatchQueue.main.async {
                    
                }
            }))
        }
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
}

